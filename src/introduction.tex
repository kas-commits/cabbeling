%======================================================================
\chapter{Introduction \& Historical}
%======================================================================

%----------------------------------------------------------------------
\section{The equation of state}
%----------------------------------------------------------------------

The equation of state in the context of seawater dynamics relates the density,
$\rho$, to temperature $T$, salinity $S$, and pressure $p$.
\citeauthor{Millero1980} details the UNESCO equation (named so because the
aforementioned organization's Joint Panel on Oceanographic Tables and Standards
recommend its use for the oceanographic community) \cite{Millero1980}. This
equation is rather computationally expensive for what it is worth, so efforts
have been made to approximate this equation of state for use in simulations. These
approximations can be made domain-specific to prevent redundant aspects of the
equation. \citeauthor{Brydon1999} provides one such approximation where density
is no longer a parameter, which is a good approximation for shallow-water
dynamics \cite{Brydon1999}.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{equations_of_state}
    \caption{A plot of density (shifted by a fixed value) versus temperature.
    Both the full equation of state (blue) and the quadratic approximation of
    this equation (red) are shown. The temperature scale is shifted by $T^*$,
    the temperature of maximum density.}
    \label{fig:eqn_of_state}
\end{figure}

Figure \ref{fig:eqn_of_state} demonstrates the accuracy of this quadratic
approximation. Here, the salinity is taken as $S=0$ to capture freshwater
dynamics. The figure shows that, for temperatures below $T^*$, the equation of
state is essentially quadratic but the approximation becomes less accurate as
$T$ grows further away from $T^*$. Through the rest of this report the terms
``equation of state`` and density, $\rho$, are used interchangeably.

%----------------------------------------------------------------------
\subsection{A note on salinity}
%----------------------------------------------------------------------

Salinity can be a complex factor in realistic descriptions of seawater
and oceanographic modeling in general. Having non-zero salinity also makes 
density dependent on multiple factors, which makes interpretation of results 
more difficult. For these reasons we chose to set $S=0$, which translates to 
considering only freshwater dynamics. For an example where the salinity of the water is
non-negligable with respect to temperature and $\rho$, see \textcite[][Fig.
2]{Cortes2020} which reports on a high latitude lake in early Spring.
% Marek: may be quote something that discusses lakes with non negligible salinity.  I will send you one possible paper.

%----------------------------------------------------------------------
\section{Stratified flow}
%----------------------------------------------------------------------

Fluid will flow according the Navier-Stokes equations until it reaches
energetic equilibrium. Non-equilibrium states are often called unstable and fluid
will flow differently according to the type of instability observed. Different
instabilities produce different patterns of fluid evolution, but can also be combined for
new types of instabilities. Kelvin-Helmholtz (herein called `KH') instability
occurs when there exists a velocity shear across a continuous interface of two
different fluids. \textcite[][chap 12]{Kundu2010} provide more details. 
% Marek: Kundu's book is a good standard reference for instabilities.  Quote it here and in the next paragraph.

Rayleigh–Taylor (herein called `RT') instability is another common type of
instability wherein two fluids of different densities have a continuous
interface and the heavier fluid overlies the lighter fluid. A simple example
would be adding liquid mercury to a beaker of water. As the less dense fluid,
the water will push the mercury out of the way as it rises above it to reach
equilibrium in the presence of gravity. Cabbeling is another, more complex, example of a
Rayleigh-Taylor instability \cite[][chap 12]{Kundu2010}.

%----------------------------------------------------------------------
\section{Cabbeling}
%----------------------------------------------------------------------

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{cabbeling_diagram}
  \caption{Figure 1 in \cite{Hanson2021}. The density of pure water as a
  function of temperature, with the highest density temperature labelled
  $T^*$.}
  \label{fig:cabbeling}
\end{figure}

As we see in Figure \ref{fig:cabbeling}, the equation of state is not a
monotonic function (indeed it is concave). For simplicity, consider a scenario
in which you prepare two water samples - one with temperature $T_1 < T^*$ and
the second with $T_2 > T^*$ such that $\rho(T_1) = \rho(T_2)$. If we allow
these two samples to thoroughly mix together, the resultant water will have a new
temperature $T_{\text{new}}$ where $T_1 < T_{\text{new}} < T_2$. Consequently,
this will result in a new density $\rho(T_{\text{new}}) > \rho(T_1) =
\rho(T_2)$.

We can therefore mix two samples of water with the same density (but different
temperatures) and get a resultant denser fluid. This phenomenon is known as
cabbeling and in the presence of gravity will affect the flow of the water
parcels in a non-trivial way. According to \textcite{Foster1972}, \textcite{Witte1902} was the first person to point out the possible importance of this process in the ocean:

\begin{displayquote}[\citeauthor{Foster1972} \citeyear{Foster1972}][]
Witte thought that the process would be so effective that the sinking could give rise to a rippled or choppy appearance of the sea surface, which in German nautical terminology has been designated ``Kabbelung.''
\end{displayquote}

% Marek: a reference here?  wiki has a very old German reference.
In order for cabbeling to be observed however, we
require a reservoir of water with temperature below $T^*$. This is to ensure
that the averaged temperature of the new parcel is denser than either parent
parcel. We know that $T^*$ is near 4$^\circ$C, so there exists a certain, limited
range of temperatures where cabbeling can be observed, which we call the
``cabbeling regime.''

Due to the actual value of $T^*$ for water, cabbeling is only prominent for
freshwater in cold, near-freezing or partially frozen lake beds. These scenarios
make for interesting case study however, especially during the transition
period from winter to spring, where the sun heats the top of a frozen-over
lake bed inciting stratified flow.

%======================================================================
\section{Density-tracer scatter plots}
%======================================================================

A bivariate histogram is a histogram of two variables over their respective
ranges of values, creating a three-dimensional figure. A quantitative scatter
plot of a bivariate histogram is a two dimensional figure where a colormap is
used to represent the height of the bivariate histogram in a specific bin.

A flow tracer is any property of a fluid that can be used to track flow,
magnitude, direction or circulation. Examples of tracers would be total kinetic
energy, density, pressure, temperature, individual components of velocity, etc.
Active tracers are tracers which appear in the equation of motion of a fluid,
implying that a change in the distribution of an active tracer is affecting the
motion of the fluid. Examples of active tracers are density and temperature.
Passive tracers do not appear in the equation of motion and thus do not
directly influence flow. An example of a passive tracer is the concentration of 
a non-reactive colored dye (a technique used in many fluid experiments).
% Marek: this is confusing, I would say dye is a passive tracer, used in the lab as well as some field experiments, and there is no confusion.

In section 4 of \cite{Penney2020}, \citeauthor{Penney2020} examine the usage
and interpretation of density-tracer quantitative scatter plots as a diagnostic
tool to describe the diapycnal fluxes of a passive tracer. Let $\phi$
represent the concentration of a passive tracer. The article argues that, under
the condition of reversible mixing, a fluid parcel should remain in the same
location in density-tracer ($\rho\phi$) space regardless of its displacement or
geometrical diffusion in physical space. Therefore any change which occurs to
these plots is a result of the irreversible process of mixing caused by an
explicit diffusion term.
% Marek: references? Or is that in the next paragraph? If so why a new paragraph?
Noting a few real-world examples mentioned in the paper,
\citeauthor{Tomczak1981} and \citeauthor{Teramoto1993} used a
temperature-salinity diagram to qualify mixing of large water masses in the
ocean \cites{Tomczak1981}{Teramoto1993}. \citeauthor{Loder1981} and
\citeauthor{Officer1981} used tracer-salinity plots to classify regions of
sources and sinks in estuaries (partially enclosed bodies of brackish water)
\cites{Loder1981}{Officer1981}. Lastly, \citeauthor{Tilmes2006} and
\citeauthor{Plumb2007} used tracer-tracer diagrams to examine compact
relationships between different atmospheric tracers
\cites{Tilmes2006}{Plumb2007}.


% %----------------------------------------------------------------------
% \section{Numerical methods to PDEs}
% %----------------------------------------------------------------------

% There are many ways to solve a differential equation numerically. The method
% that provides some of the fastest convergence to the true solution is the
% Spectral method (exponential convergence). As the name implies, the idea behind
% these methods involve applying the spectral theorem on the solution to
% decompose it into a suitable basis over the Sobolev space which describes the
% solution space. This method is often paired with Fourier analysis wherein a
% Fourier series (commonly the Chebysev polynomials) are used for the basis. For
% details see \textcite{Boyd2000}.

% Another and possibly more popular technique is using Finite element methods.
% These methods are a specific form the Galerkin method. Galerkin methods solve
% the PDE numerically by solving the weak formulation of the problem in a
% finite-dimensional Hilbert space, wherein a solution is picking from this
% Hilbert space such that it is orthogonal to the true solution in its
% infinite-dimensional Hilbert space. These methods dominate many Engineering
% applications due to how easy it is to deal with complex topographies. This is
% because these methods solve the PDE locally over a `finite element' and the
% result over each local element is stitched together to create a global solution.

% There are two flavors of these methods - Continuous Galerkin (herein called
% `CG') and Discrete Galerkin (herein called `DG') methods. CG methods enforce
% the restriction that the local solutions over each finite element forms a
% continuous (but not necessarily smooth) approximate solution. DG methods do not
% enforce this property. These methods converge at a rate dependent on the order
% of the polynomial used in the finite dimensional Hilbert space. To avoid
% over-fitting oscillatory behavior, these methods typically do not use
% polynomials higher than 6th order. For more details see \cite{Brenner2008}
% and for an implementation in the field see \cite{Fringer2006}.

% The software used to evolve the model used for this report is the SPINS library
% courtesy of \textcite{Subich2013}. SPINS is a parallel C++ library designed to
% evolve the Navier-Stokes equations in two and three dimensions. The focus of
% this library is solving incompressible fluids over relatively smooth
% topographies. As such, spectral collocation methods are chosen as a suitable
% numerical technique.
