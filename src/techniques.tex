%======================================================================
\chapter{Techniques}
%======================================================================

%----------------------------------------------------------------------
\section{Governing equations}
%----------------------------------------------------------------------

To represent velocity let us define
\begin{equation}
  \va{u}(\va{x},t) = \qty(u(\va{x},t), v(\va{x},t), w(\va{x},t))
  \qq{where}
  \va{x} = \qty(x,y,z)
\end{equation}
We use the same governing equations as in \cite{Hanson2021}. Namely, we solve
the Navier-Stokes equations using the Boussinesq approximation. This
approximation assumes that density changes in the fluid can be neglected except
in the buoyancy term where $\rho$ is multiplied by the acceleration due to gravity, $g$. Recall that the
continuity equation in the context of in-compressible flow is
\begin{equation}
  \frac{1}{\rho}\frac{D\rho}{Dt} + \div{\va{u}} = 0
\end{equation}
The result of the Boussinesq approximation means we assume that
\begin{equation}
  \frac{1}{\rho}\frac{D\rho}{Dt} \ll \div{\va{u}}
\end{equation}
to get the following set of governing equations:
\begin{align}
  \div{\va{u}} &= 0\\
  \frac{D\va{u}}{Dt} &= -\frac{1}{\rho_0}\grad{p} + \nu\laplacian{\va{u}} -
  \frac{\rho(T)}{\rho_0}g\hat k\\
  \frac{DT}{Dt} &= \kappa\laplacian{T}
\end{align}
where $\frac{D}{Dt}$ is the material derivative, defined as
\begin{equation}
  \frac{D}{Dt} \coloneqq \pdv{t} + \va{u}\cdot\grad
\end{equation}
$p$ is the pressure, $\rho$ is the density, $\nu$ is the kinematic viscosity,
$\kappa$ is the thermal diffusivity, $T$ is the temperature and $g$ is the
acceleration due to gravity. Here $\rho(T)$ is the equation of state as
specified by \textcite{Brydon1999}.

%----------------------------------------------------------------------
\section{Numerical methods and initialization}
%----------------------------------------------------------------------

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{Stratified}
  \caption{Visual representation of the initial background velocity field,
  $U$, the temperature field $T$ and the density field $\rho$ of the
  simulation performed.}
  \label{fig:stratified}
\end{figure}

Figure \ref{fig:stratified} visually demonstrates the qualitative setup of the
simulation to be performed for this report. The first panel demonstrates the
fact we have KH instability. Velocity is a three-component vector dependent on the
spacial coordinates $x,y,z$ and the time $t$. The initial velocity of the fluid
can be described as
\begin{equation}
  \va{u}(\va{x},0) = \mqty(U(z), 0, 0)
\end{equation}
where $U(z)$ is
\begin{equation}
U(z) = \frac{\Delta u}{2}\tanh(\frac{z-\frac{3}{4}L_z}{L_{\text{shear}}})
\end{equation}
The initial temperature field is defined similarly as
\begin{equation}
    T(z) = T_0 + \frac{\Delta T}{2}\qty[1 + \tanh(\frac{z-\frac{3}{4}L_z}{L_{\text{shear}}})]
\end{equation}
The middle panel shows the fact that, initially, the water has one of two
possible temperatures, $T_1$ and $T_2$, where $T_1 > T_2$. Lastly, our initial
density field is such that the maximum density fluid is sandwiched between two
different densities which are roughly equal. For a full description of these
values please see table \ref{table:hyper}.

\begin{table}
  \centering
  \begin{tabular}{c c c}
    \hline\hline
    Label & Description & Value\\
    \hline
    $L_x$ & Length of tank ($m$) & 0.384\\
    $L_y$ & Width of tank ($m$) & 0.384\\
    $L_z$ & Height of tank ($m$) & 0.512\\
    $L_{\text{mix}}$ & Width of mixed region ($m$) & 0.384\\
    $L_{\text{shear}}$ & Shear length scale ($m$) & 0.01\\
    $N_x$ & Number of points in $x$ & 512\\
    $N_y$ & Number of points in $y$ & 512\\
    $N_z$ & Number of points in $z$ & 1024\\
    $T_0$ & Baseline temperature ($^\circ$C) & 0.5\\
    $\Delta T$ & Initial difference in $T(z)$ ($^\circ$C) & 6.0\\
    $\Delta u$ & Initial difference in $u$ (m/s) & 0.01\\
    % $\Delta x$ & Horizontal transition length ($m$) & 0.01\\
    $\nu$ & Kinematic viscosity ($m^2/s$) & $1\times 10^{-6}$\\
    $\kappa_T$ & Diffusivity of temperature ($m^2/s$) & $0.14285\times 10^{-6}$\\
    type\_x & Grid type in $x$ & Fourier\\
    type\_y & Grid type in $y$ & Fourier\\
    type\_z & Grid type in $z$ & Free slip\\
    \hline\hline
  \end{tabular}
  \caption{A list of the relevant values of parameters used for the deeper
  simulation.}
  \label{table:hyper}
\end{table}

%----------------------------------------------------------------------
\section{Deriving diagnostics}
%----------------------------------------------------------------------

The first diagnostic we denote is the kinetic energy, which we will define
without the factor of $\rho_0$ as done in \cite{Hanson2021} following standard (if confusing) fluid mechanics convention:
\begin{equation}
  ke(x,y,z,t) = \frac{1}{2}\qty(u^2+v^2+w^2)
\end{equation}
We've initialized the system with an initial velocity with $v=0$ and $w=0$,
hence we define a ``background'' velocity as
\begin{equation}
  U(z,t) = \frac{1}{L_xL_y}\int_{0}^{L_x}\int_{0}^{L_y}u(x,y,z,t)\dd{x}\dd{y}
\end{equation}
as such we can define the background kinetic energy as
\begin{equation}
  ke_{u_b} = \frac{1}{2}U(z,t)^2
\end{equation}
and the perturbation kinetic energies as
\begin{equation}
  ke_{u_p} =
  \frac{1}{2}\qty(u-U)^2,\qq{}ke_{v}=\frac{1}{2}v^2,\qq{}ke_{w}=\frac{1}{2}w^2
\end{equation}
These kinetic energies, however, are still functions of all of $x,y,z,t$.
Horizontally averaging them out we can get that
\begin{align}
  \expval{ke_{u_b}}_{xy}(z,t) &=
  \frac{1}{L_xL_y}\int_{0}^{L_x}\int_{0}^{L_y}ke_{u_b}\dd{x}\dd{y}\\
  \expval{ke_{u_p}}_{xy}(z,t) &=
  \frac{1}{L_xL_y}\int_{0}^{L_x}\int_{0}^{L_y}ke_{u_p}\dd{x}\dd{y}\\
  \expval{ke_{v}}_{xy}(z,t)&=
  \frac{1}{L_xL_y}\int_{0}^{L_x}\int_{0}^{L_y}ke_{v}\dd{x}\dd{y}\\
  \expval{ke_{w}}_{xy}(z,t) &=
  \frac{1}{L_xL_y}\int_{0}^{L_x}\int_{0}^{L_y}ke_{w}\dd{x}\dd{y}
\end{align}

%======================================================================
\section{A brief note on MPI}
%======================================================================

In the pursuit of executing a program using more than a single, serialized
thread, multiple computational models have been designed. The most well-known
of these models are data parallelism, shared memory, and message passing. In
the context of numerical computation, Data parallelism is best known in the
context of `vecotization.' Under this model, only the data is parallelized and
the instructions performed on the data remains constant for every thread. This
model is often used implicitly through compiler directives such as OpenMP.
Rather than parallelizing the data, we could instead specify the control flow
of the process nodes. This is the shared memory model and is the most up-front
with regards to the programer, requiring care to avoid data races and related
issues. Message passing instead uses the notion of process nodes, each with its
own locally encapsulated data and which communicate by sending and receiving
messages. MPI (the Message Passing Interface) is a specific realization of this
model. Message passing is the chosen model used in SPINS \cite{Subich2013}.

In order to understand the C++ implementation we must go through the key
methods used to pass information between the process nodes \footnote{In our
case, the `process nodes' were simply the threads of a multi-core CPU. In
comparison, supercomputers often configure MPI such that nodes are entire CPUs,
which are multi-threaded themselves and are further parallelised using another
model}. In order to pass information between nodes, cooperation is required both
by the sender and the receiver. As the scale of the graph grows, it becomes
increasingly cumbersome to handle this communication. MPI provides helper
functions for common operations.

There are two functions (and close relatives of them) which concern us: 
\texttt{MPI\_Bcast} and \texttt{MPI\_Reduce}. As their names suggest, 
\texttt{MPI\_Bcast} and \texttt{MPI\_Reduce} implement
the notions of broadcasting and reducing respectively. Broadcasting is the act
of sending local data from a single node to all nodes. This is useful whenever
a global result is calculated or discovered in a local node. Reduce acts over a
graph structure by recursively reducing two nodes into one until only one node
remains, where the value of the new node is the result of some function with
the previous nodes' values as input\footnote{If you pay close attention to the
naming of these (and many other) helper functions, you will notice that they
share their names with common functional-style programming techniques. This is
not an accident! Processor nodes are modeled as a graph structure (or in
simpler cases a list), which is a core abstract data type in functional
programming.}. For example, if we chose our function as
$f(a,b) = a+b$ and our graph is just an ordered list $\qty(a_i)$, reduce will
return the value $\sum_{i}a_i$. MPI supports many
operators, such as max, min, sum, subtract, and others. \texttt{MPI\_Allreduce} is a close
relative of \texttt{MPI\_Reduce}, where it will save the result of reduce to
all processor nodes, rather than just 1 node. For a visual illustration of
these two methods, see figure \ref{fig:mpi_reduce}.

\begin{figure}
  \begin{center}
    \begin{tikzpicture}[every node/.style={circle,thick,draw}]
      \node (1)  at (5,2) {0};
      \node (2)  at (5,3) {1};
      \node (3)  at (5,4) {2};
      \node (4)  at (5,1) {3};
      \node (5)  at (5,0) {4};
      \node (6)  at (8,2) {0};
      \node (7)  at (8,3) {1};
      \node (8)  at (8,4) {2};
      \node (9)  at (8,1) {3};
      \node (10) at (8,0) {4};
      \node (11) at (11,2) {0};
      \node (12) at (11,3) {1};
      \node (13) at (11,4) {2};
      \node (14) at (11,1) {3};
      \node (15) at (11,0) {4};
      \node (16) at (13,2) {0};
      \node (17) at (6.5,2) {};
      \node (18) at (2,2) {0};
      \node (19) at (2,3) {1};
      \node (20) at (2,4) {2};
      \node (21) at (2,1) {3};
      \node (22) at (2,0) {4};
      \node (23) at (0,2) {0};
      \draw (1) -- (17);
      \draw (2) -- (17);
      \draw (3) -- (17);
      \draw (4) -- (17);
      \draw (5) -- (17);
      \draw (6) -- (17);
      \draw (7) -- (17);
      \draw (8) -- (17);
      \draw (9) -- (17);
      \draw (10) -- (17);
      \draw (11) -- (16);
      \draw (12) -- (16);
      \draw (13) -- (16);
      \draw (14) -- (16);
      \draw (15) -- (16);
      \draw (18) -- (23);
      \draw (19) -- (23);
      \draw (20) -- (23);
      \draw (21) -- (23);
      \draw (22) -- (23);
      %  \draw[decoration={
      %  text along path,
      %  text={MPI},
      %  text align={center},
      %  raise={0.2cm}
      %  }, decorate] (1) -- (6);
      %  \draw[decoration={
      %  text along path,
      %  text={Allreduce},
      %  text align={center},
      %  raise={-0.5cm},
      %  }, decorate] (1) -- (17);
    \end{tikzpicture}
  \end{center}
  \caption{(left) A visual representation of the message passing that happens
  during an \texttt{MPI\_Bcast} call. (middle) A visual representation of the
  message passing that happens during an \texttt{MPI\_Allreduce} call. (right)
  A visual representation of the message passing that happens during an
  \texttt{MPI\_Reduce} call. All lines should be interpreted as message passing,
  where the left node is the sender and the right node is the receiver.}
  \label{fig:mpi_reduce}
\end{figure}

%======================================================================
\section{QSP Algorithm}
%======================================================================

Consider a tracer labeled $\phi$. We wish to find the joint
probability distribution over $\phi$ and $\rho$ i.e. we want a discrete
expression of $p(\phi, \rho)$ in matrix form. To get this matrix we either need
to iterate over coordinate space or over $\phi\rho$ space.

In section 4.1 of \cite{Penney2020}, \citeauthor{Penney2020} details how to
calculate the bin centers along with the size of each bin (assuming each bin is
equally sized) . The paper also defines the bin weight as
\begin{equation}
  \begin{aligned}
    W_{ij}(t)
    &= W(\rho_i, \phi_i, t)\\
    &= \frac{1}{V}\sum_{x,y,z} I_{ij}(\rho, \phi, t)\Delta V
  \end{aligned}
\end{equation}
\ignorespacesafterend
where $\Delta V = \Delta x\Delta y\Delta z$ and $I_{ij}$ is the indicator
variable function which is 1 when the $(\rho_i,\phi_i)$ pair is in the
$(i,j)$th bin and 0 otherwise. Note that while this definition is
mathematically correct, it should not be implemented \emph{ad hoc} in order to
calculate the bin weights. To see why, consider that for every single bin
$W_{ij}(t)$, under this method we would need to iterate through all coordinate
space. The complexity of such an algorithm would therefore be of order
$\order{N_xN_yN_zN_\rho N_\phi}$.

If we instead iterate through coordinate space first, we can avoid scaling the
complexity by the number of total bins by abusing the fact that we have equally
spaced bins! As we iterate through coordinate space, we can calculate the
correct bin to send the tracer to by the following equation:
\begin{equation}
  I = \text{floor}\qty(\frac{\phi_i - \phi_{\text{min}}}{h})
\end{equation}
where
\begin{equation}
  h = \frac{\phi_{\text{max}} - \phi_{\text{min}}}{N_\phi}
\end{equation}
and $\phi_{\text{max}}$ and $\phi_{\text{min}}$ are the maximum and minimum
values of the edge bins, which are specified by the user. This can be trivially
derived by rearranging the linear equation used to generate a discrete uniform
mesh. By using this method, we don't need to iterate through tracer-tracer
space to find the right bin. This cuts out 2 inner loops and reduces complexity
to $\order{N_xN_yN_z}$. To demonstrate the difference in speed, if we wish to
use 40 bins for each tracer, this method would be on the order of $40\times 40
= 1600$ times faster.

Algorithm \ref{alg:QSP} implements the efficient method of calculating the bin
weights. These weights, however, are not normalized. We can modify the
algorithm to yield normalized results by replacing $1$ with
$\frac{1}{N_xN_yN_y}$ on the last line\footnote{We also could have simply
normalized the matrix after the loops complete but this is worse in
performance} This however would decreases accuracy by creating floating point
round-off error for every addition, which is not ideal. We can reduce the number
of floating point operations by instead normalizing the matrix after its
construction by dividing each term by $N_xN_yN_z$. While this adds increased
complexity, it will be a useful trade-off for accuracy\footnote{Assuming that 
$N_\phi N_\rho \ll N_xN_yN_z$}.

\begin{algorithm}
  \label{alg:QSP}
  \caption{QSP}
  \DontPrintSemicolon
  \KwInput{$T$, $S$, $T_{\text{max}}$, $S_{\text{max}}$, $T_{\text{min}}$,
  $S_{\text{min}}$, $N_T$, $N_S$, $N_x$, $N_y$, $N_z$}
  \KwOutput{$W$}
  $W \gets \mathbb{0}_{N_S\times N_T}$\\
  $h_S \gets \qty(S_{\text{max}} - S_{\text{min}}) / N_S$\\
  $h_T \gets \qty(T_{\text{max}} - T_{\text{min}}) / N_T$\\
  \For{$i = 0:N_x$}{
    \For{$j = 0:N_y$} {
      \For{$k = 0:N_z$} {
        $I_T \gets \text{floor}\qty[\qty(T(i,j,k) - T_{\text{min}}) / h_T]$\\
        $I_S \gets \text{floor}\qty[\qty(S(i,j,k) - S_{\text{min}}) / h_S]$\\
        $I_T \gets I_T \geq N_T$ ? $N_T$ : $I_T$ \\
        $I_T \gets I_T < 0$ ? $0$ : $I_T$ \\
        $I_S \gets I_S \geq N_S$ ? $N_S$ : $I_S$ \\
        $I_S \gets I_S < 0$ ? $0$ : $I_S$ \\
        $W(I_S, I_T) \gets W(I_S,I_T) + 1$
      }
    }
  }
\end{algorithm}

%======================================================================
\section{C++ usage and justification}
%======================================================================

In its direct numerical simulation configuration, SPINS
will output binary files storing the values of $u,v,w$ and other fields if
requested by the user. SPINS uses the double intrinsic data type from C++. On
\texttt{x86\_64} machines this data type is 64 bits wide. On a 512x512x1024 grid (used for the simulations reported on in this report)
this means every field requires $1.7180\times 10^{10}$ bits of storage or
$2.1475$ Gigabytes. To plot kinetic energy versus density, we require all three
velocity fields as well as $\rho$, so for every time step we need to load
$8.59$ Gigabytes of data into memory to compute the QSP.

Matlab is a dynamic interpreted language with a garbage collector.
Consequently, Matlab doesn't provide enough low-level features to communicate
to the garbage collector and virtual machine that we wish to overwrite existing
allocated memory instead of allocating new memory. This means that for every
timestep, the Matlab virtual machine will call the operating system's kernel to
allocate $8.59$ Gigabytes, load the data from disk to memory in serial, do work
on that data, then call the kernel for new memory while also running the
garbage collector to free the previous memory block, with no guarantee on when
this occurs.

Internal testing revealed that implementing algorithm \ref{alg:QSP} in Matlab
took prohibitively long to run. For this reason, the code written to make the
QSP figures was implemented in C++. A detailed overview of the code can be
found in Appendix \ref{app:Source_code}

