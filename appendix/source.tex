%======================================================================
\chapter{Source Listings}\label{app:Source_code}
%======================================================================

This appendix section covers the C++ code design and certain important design
decisions. The full code used to create the figures shown in this report can be
found in the following merge request:

\url{https://git.uwaterloo.ca/SPINS/SPINS_main/-/merge_requests/12}

%======================================================================
\section{Input design}
%======================================================================

Input is handled through the usual SPINS way of adding options in the main
function and adding key-value declarations in \texttt{spins.conf}. Listing
\ref{code:input} provides a sample of the code used to handle user options.
Note that the user specifies the desired tracer through \texttt{char}
variables, although a combination of enums and strings also could have been
used, albeit with more boilerplate code required (but the added benefit of
type safety).

\begin{listing}
\caption{Options which get set at runtime in the file \texttt{spins.conf}.
  Options are largely handled through the Boost.Program\_options library}
\label{code:input}
\begin{minted}[linenos=true, frame=lines, breaklines, breakanywhere]{c++}
char T1_name, S1_name;
double T1_max, T1_min, S1_max, S1_min;
std::string QSP_filename;
unsigned int NS, NT;
add_option("do_hist",&do_hist,false,"Create QSP Data?");
add_option("T1",&T1_name,'t', "Name of tracer 1 for QSP.  Valid values are t (for rho),u,v,w,T (for temp) or k for K.E.");
add_option("S1",&S1_name,'w', "Name of tracer 2 for QSP.  Valid values are t (for rho),u,v,w,T (for temp) or k for K.E.");
add_option("T1_max",&T1_max,std::numeric_limits<double>::max(), "Maximum explicit bin for T1 in QSP.");
add_option("T1_min",&T1_min,std::numeric_limits<double>::min(), "Minimum explicit bin for T1 in QSP.");
add_option("S1_max",&S1_max,std::numeric_limits<double>::max(), "Maximum explicit bin for S1 in QSP.");
add_option("S1_min",&S1_min,std::numeric_limits<double>::min(), "Minimum explicit bin for S1 in QSP.");
add_option("QSP_filename",&QSP_filename,"QSP_default", "Filename to save data to. Don't include file extension.");
add_option("NS",&NS,10,"Number of bins for tracer S");
add_option("NT",&NT,10,"Number of bins for tracer T");
\end{minted}
\end{listing}

%======================================================================
\section{Output design}
%======================================================================

Dealing with output was a more considered design choice since many options
could be taken. Ultimately, we wish for the code to provide all the required
data needed to use higher-level scripting languages like Python and Matlab,
without requiring the binary field data from SPINS. The file format decided on
was csv for its universal support in the aforementioned languages. 

We also need to convey the information of
the min and max values of each tracer so the user can create a linspace in
their scripting language of choice. This is accomplished by adding an
additional row at the beginning of the csv file with semantic meaning; the
first value in the row is T1\_max, followed by T1\_min, S1\_max and finally
S1\_min. Many csv parsers encounter errors if the csv data is not rectangular,
so padding is added on this row to match the column number of matrix
underneath. A sample of the final result is shown in listing \ref{code:output}

\begin{listing}
\caption{Master MPI node (i.e. node rank 0) writes to a unique filename based
  on the timestep.}
\label{code:output}
\begin{minted}[linenos=true, frame=lines, breaklines, breakanywhere]{c++}
if (local_rank == 0) {
  MPI_Reduce(local_hist, glob_hist, // send and receive buffers
             NS * NT, MPI_INT,      // count and datatype
             MPI_SUM, 0,            // Reduction and proc
             MPI_COMM_WORLD);       // Communicator
  filename += "." + boost::lexical_cast<string>(plotnum) + ".csv";
  std::fstream outfile;
  outfile.open(filename.c_str(), std::ios_base::out);
  if (outfile.is_open()) {
    outfile<<T1_max<<','<<T1_min<<','<<S1_max<<','<<S1_min;
    for (int i = 4; i < NT; i++) {
      outfile << ',' << 0;
    }
    outfile << std::endl;
    for (int ii = 0; ii < NS; ii++) {
      outfile << glob_hist[index(ii, 0, NS, NT)];
      for (int jj = 1; jj < NT; jj++) {
        outfile << ',' << glob_hist[index(ii, jj, NS, NT)];
      }
      outfile << std::endl;
    }
  }
} else {
  MPI_Reduce(local_hist, NULL, // send and receive buffers
             NS * NT, MPI_INT, // count and datatype
             MPI_SUM, 0,       // Reduction and root proc
             MPI_COMM_WORLD);  // Communicator
}
\end{minted}
\end{listing}

%======================================================================
\section{Data design}
%======================================================================

As mentioned in the body of this report, each field is over $2.1$ Gigabytes of
data in memory, so using pass-by-value semantics is not acceptable. For this
reason we use pass-by-reference to handle passing data between code boundaries
like function calls. Listing \ref{code:func} shows the function signature of
our QSP method.

\begin{listing}
\caption{The function signature. t,u,v and w are the temperature, first, second
  and third component of velocity respectively. Plotnum is specific timestep we
  are in, and the rest of the variables are described in listing
  \ref{code:input}}
\label{code:func}
\begin{minted}[linenos=true, frame=lines, breaklines, breakanywhere]{c++}
void QSPCount(const TArrayn::DTArray &t, const TArrayn::DTArray &u,
              const TArrayn::DTArray &v, const TArrayn::DTArray &w,
              const char T1_name, const char S1_name, const int NS,
              const int NT, double T1_max, double S1_max, double T1_min,
              double S1_min, const int Nx, const int Ny, const int Nz,
              string filename, const int plotnum) {
  ...
}
\end{minted}
\end{listing}

As you can see, we pass all available fields by reference to the function. This
is very cheap since pointers are a very small datatype and we use const to
ensure the user that we don't mutate the fields we are given. Once we know
which fields we need for computation, we can create raw pointers which will
point to the correct field. The logic behind this can be found in listing
\ref{code:pointer}. Some explanation is needed for the T1\_name variable. As
its name suggests, it captures the name of the tracer the user wants. This
indirection is needed since, obviously, the user can't specify the machine
address of a yet-to-be-allocated variable in a configuration file. If the user
wants to use kinetic energy, they specify the character `k', which will get
used in the conditional branch later in the codebase.

\begin{listing}
\caption{Branch code to decide what to point to.}
\label{code:pointer}
\begin{minted}[linenos=true, frame=lines, breaklines, breakanywhere]{c++}
const TArrayn::DTArray *T1_ptr = NULL, *S1_ptr = NULL;

switch (T1_name) {
  case 'u':
    T1_ptr = &u;
    break;
  case 'v':
    T1_ptr = &v;
    break;
  case 'w':
    T1_ptr = &w;
    break;
  case 'k':
    break;
  case 't':
    T1_ptr = &t;
    break;
  case 'T':
    T1_ptr = &t;
    break;
  default:
    return;
}
\end{minted}
\end{listing}

%======================================================================
\section{Calculating Minimum and Maximum values}
%======================================================================

In the case where the user does not specify what the minimum and maximum value of the
bins should be for each tracer, a branch of the code is executed to calculate the
global maximum/minimum values as default behavior. In order to achieve this, we
parallel the data into quadrants for each process node, calculate the local
minimum and maximum values on each quadrant, and compare all the local values to find
the true global values. This functionality is already built into the data class used
in SPINS. This however cannot be used if the user specified kinetic energy or $\rho$, 
since they are non-linear by nature and need hand-rolled loops. Listing \ref{code:minmax}
is a sample of the code used to find the local values, and listing \ref{code:minmaxjoin}
shows how we use MPI calls to communicate to every process node the global results.

\begin{listing}
\caption{Calculate the local minimum and maximum of kinetic energy and $\rho$.}
\label{code:minmax}
\begin{minted}[linenos=true, frame=lines, breaklines, breakanywhere]{c++}
for (int i = i_low; i <= i_high; i++) {
  for (int j = j_low; j <= j_high; j++) {
    for (int k = k_low; k <= k_high; k++) {
      double ke_current = 0, tmp = 0;
      if (Nx > 1) {
        tmp = u(i, j, k);
        ke_current += tmp * tmp;
      }
      if (Ny > 1) {
        tmp = v(i, j, k);
        ke_current += tmp * tmp;
      }
      if (Nz > 1) {
        tmp = w(i, j, k);
        ke_current += tmp * tmp;
      }
      ke_current = 0.5 * ke_current;
      double rho_current = eqn_of_state_t(t(i, j, k));
      ke_max = ke_current > ke_max ? ke_current : ke_max;
      ke_min = ke_current < ke_min ? ke_current : ke_min;
      rho_max = rho_current > rho_max ? rho_current : rho_max;
      rho_min = rho_current < rho_min ? rho_current : rho_min;
    }
  }
}
\end{minted}
\end{listing}

\begin{listing}
\caption{Communicate the global values to all process nodes for use in the main QSP loop.}
\label{code:minmaxjoin}
\begin{minted}[linenos=true, frame=lines, breaklines, breakanywhere]{c++}
double glob_ke_max, glob_ke_min, glob_rho_max, glob_rho_min;
MPI_Allreduce(&ke_max, &glob_ke_max, 1, MPI_DOUBLE, MPI_MAX,
              MPI_COMM_WORLD);
MPI_Allreduce(&ke_min, &glob_ke_min, 1, MPI_DOUBLE, MPI_MIN,
              MPI_COMM_WORLD);
MPI_Allreduce(&rho_max, &glob_rho_max, 1, MPI_DOUBLE, MPI_MAX,
              MPI_COMM_WORLD);
MPI_Allreduce(&rho_min, &glob_rho_min, 1, MPI_DOUBLE, MPI_MIN,
              MPI_COMM_WORLD);
\end{minted}
\end{listing}

% vim: filetype=latex
