%======================================================================
\chapter{Results}
%======================================================================

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{rhoslices_colormap}
    \caption{A legend for the colormap used by figures \ref{fig:rho10to19},
    \ref{fig:rho10to40} and \ref{fig:rho39to48}. The colorbar tracks the
    equation of state (shifted by a constant, $\rho_0$).}
    \label{fig:rhoslices_colormap}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{rhoslice_samples10to19}
    \caption{Sample slices of the domain with a colormap representing the value
    of $\rho$. The first row is the X-Z slice and the second is the Y-Z slice.
    The times of each column are 50s, 65s, 80s and 95s (left to right). Figure
    \ref{fig:rhoslices_colormap} provides the colorbar for this figure.}
    \label{fig:rho10to19}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{rhoslice_samples10to40}
    \caption{Sample slices of the domain with a colormap representing the value
    of $\rho$. The first row is the X-Z slice and the second is the Y-Z slice.
    The times of each column are 50s, 100s, 150s and 200s (left to right) Figure
    \ref{fig:rhoslices_colormap} provides the colorbar for this figure.}
    \label{fig:rho10to40}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{rhoslice_samples39to48}
    \caption{Sample slices of the domain with a colormap representing the value
    of $\rho$. The first row is the X-Z slice and the second is the Y-Z slice.
    The times of each column are 195s, 210s, 225s and 240s (left to right). The
    arrows point towards the pocket of unmixed, less dense fluid. Figure
    \ref{fig:rhoslices_colormap} provides the colorbar for this figure.}
    \label{fig:rho39to48}
\end{figure}

Figure \ref{fig:rho10to19} provides an early view of instability as expressed
in the density field. For the sake of maximizing space, the colorbar for this figure,
as well as figures \ref{fig:rho10to40} and \ref{fig:rho39to48} are provided in
the separate figure \ref{fig:rhoslices_colormap}.
Panels (a)-(d) show the onset of the KH-like instability
produced as a result of the initial background velocity, $U$. Panels (e)-(h)
show the onset of RT-like instability due to gravity, which is much more
prominent in the Y-Z slice than then X-Z slice due to the absence of $U$. (d)
and (h) show the start of the sinking process.

Figure \ref{fig:rho10to40} shows a long-term view of the irreversible mixing
developing through the domain. Panels (d) and (h) show development beginning in
the bottom of the domain, while panels (c) and (g) show development beginning
in the top of the domain.

Figure \ref{fig:rho39to48} takes a more detailed view of late-onset
development. The way in which mixing occurs in the deeper part of the domain is
non-trivial and so arrows are provided to help track the phenomenon. We can see
the mixed fluid being injected underneath the unmixed fluid, encapsulating it
from all directions and shrinking it over time. This is referred to
entrainment. \textcite[][chap 13.10]{Kundu2010} provide more details.
% Marek: may be mention that this is referred to as ``entrainment" in the turbulence literature and refer to Kundu's book?

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{karim_kez_new_pic}
    \caption{(a) The horizontally averaged background kinetic energy,
    $\expval{ke_{u_b}}_{xy}(z,t)$, scaled by its maximum value. (b) The
    horizontal average of the perturbation kinetic energy,
    $\expval{ke_{u_p} + ke_{v} + ke_{w}}_{xy}(z,t)$, scaled by its
    maximum value.}
    \label{fig:kez_new}
\end{figure}

Figure \ref{fig:kez_new} is directly comparable to figure 11 in
\cite{Hanson2021}. From panel (a) we can see that the slowest moving fluid
coincides with the densest fluid (at least initially). Although the slope
visually appears smaller due to the larger domain, we observe the same smearing
effect up to time $t=70s$ by viscosity. Past this critical point however we see
a lot of differences. To begin with, while the dark section extends upward as
in the paper, we see no slowing down of the rate at which it progresses upward
(in fact the slope seems to be increasing over time). In contrast, the paper's
figure saw a spread in the $z$ direction that eventually stopped near $t=120s$.
It is worth noting that while our domain has a vertical extent of $0.512m$, the
vertical extent in \cite{Hanson2021} was only $0.128m$. We also see that near
$t=120s$ a partition of the slow-moving contiguous block begin to sink (the
spotty dark streak aimed downwards). Past $t=200s$ we see a (slight) further
decrease in the background KE about $z=0.17m$. Surprisingly, we see a local
maximum in background KE spike in the range $z=0.3m$ to $z=0.35m$.
% (Marek: by what time do you mena here)
% Marek: might be worth reminding the reader that the Hanson et al simulations had a domain with a smaller vertical extent.

Panel (b) supports the finding of panel (a) that activity can be seen almost
everywhere in the domain. We don't see the same reduction in perturbation
kinetic energy over the domain as in \cite{Hanson2021}, implying that the fluid
is still actively mixing and activity has not died out.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{cabfraczpic}
    \caption{(a) The fraction of fluid within 10\% of the maximum density,
    $cb_{10}(z,t)$, as a function of $z$ and $t$. (b) The fraction of fluid
    within 1\% of the maximum density, $cb_{1}(z,t)$, as a function of $z$ and
    $t$.}
    \label{fig:cabfracz}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{karim_cabfracz_newpic}
    \caption{Recreating of figure \ref{fig:cabfracz} where the colorbar is
    inverted.}
    \label{fig:cabfracz_new}
\end{figure}

Figure \ref{fig:cabfracz} is directly comparable to figure 12 in
\cite{Hanson2021}. As with the other figures, very little activity
occurs before $t=70s$ as we also see in the paper. Once activity starts we see a
large loss of densest fluid near $z=0.4m$ as it mixes through the domain.
Between roughly $t=70s$ and $t=120s$ the dense fluid is largely contiguous in
the domain, however after this stage we see a clear split in this denser fluid
volume in terms of direction of travel. One group is actually traveling
upwards despite the downwards force of gravity, while the other group is
sinking below the less dense fluid as expected. This indicates that the
turbulence caused during mixing is strong enough to induce local updrafts,
preventing the denser fluid from sinking.
% Marek: this merits a comment, may be say that the turbulence is enough to induce local updrafts?

It is not entirely clear what the ratio of the two volumes of two fluids is
from figure \ref{fig:cabfracz}. Figure \ref{fig:cabfracz_new} is the result of
inverting the colormap of the prior figure. In this figure it becomes a lot
more apparent that the volume of fluid moving upwards is larger than that
sinking. Another observation made much more clear through inverting the colormap
is that between roughly $t=70s$ and $t=90s$ we see a massive dip in the fraction
of fluid within 10\% of the maximum density. After which we see an increase in
this diagnostic. This implies that the beginning of the mixing process sees a
large consumption of the densest fluid but which (partially) gets replenished.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{QSP_0}
    \caption{Quantitative scatter plots between kinetic energy (labelled
    $\phi$) and the equation of state, $\rho$ (shifted by a constant) at time
    $t=0s.$}
    \label{fig:QSP0}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{QSP_39to42}
    \caption{Quantitative scatter plots between kinetic energy (labelled
    $\phi$) and the equation of state, $\rho$ (shifted by a constant) at times
    (a) 195s. (b) 200s. (c) 205s. (d) 210s.}
    \label{fig:QSP39to42}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{QSP_close_39to42}
    \caption{Quantitative scatter plots between kinetic energy (labelled
    $\phi$) and the equation of state, $\rho$ (shifted by a constant) at times
    (a) 195s. (b) 200s. (c) 205s. (d) 210s. The bounds of the domain are smaller
    here than they were in figure \ref{fig:QSP39to42}}
    \label{fig:QSPclose39to42}
\end{figure}

Figure \ref{fig:QSP0} provides a look at the initial system. Since we know what
the initial state should be this can prove useful as a sanity check. The
figure shows two main bins for $\rho$ corresponding to $\rho_1\approx\rho_2$ and
$\rho_{max}$ from figure \ref{fig:stratified}. These two densities correspond
to the volumes of fluids above and below the $\frac{3}{4}$ point where out
volume of maximum density fluid lives. We see that the larger of the two
quantities is the higher density fluid (as we expect based on figure
\ref{fig:rho10to19}). In order to capture this difference we had to omit the
section of the plot which the high density strip of fluid occupies, but
nevertheless it still exists (although in much smaller quantity).

Following the insight provided by panel (a) of figure \ref{fig:kez_new} we
decide to investigate the time range $t=195s$ to $t=210s$ in more detail. Figure
\ref{fig:QSP39to42} provides the QSP between kinetic energy and density.
Comparing to figure \ref{fig:QSP0}, we see the addition of a new volume of
fluid in the densest range of $\rho$ bins due to the cabbeling phenomenon. We
now see fluid occupying the region of $\rho\phi$ space in the middle of the
density strip across the panels we see that the fluid in the primary density bin
density strip across the panels we see that the fluid in the primary density bin
is moving out of that bin. Specifically, the faster fluid in that strip is
becoming more dense through cabbeling, while the slower fluid has yet to mix
and as such remains undisturbed in KE-$\rho$ space.

from figure \ref{fig:QSP39to42}. We can see a new observation in that the volume
of fluid occupying this region is decreasing over time, not increasing. Fluid
of fluid occupying this region is decreasing over time, not increasing. Fluid
will naturally move away from this region if it mixes with new fluid (recall that
the equation of state is concave). Thus we see that between $t=195s$ and $t=210s$
the newly created fluid with high density is mixing faster than it is being
produced
